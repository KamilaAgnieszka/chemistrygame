﻿using ChemistryGame.Data;
using ChemistryGame.Data.DTO;
using ChemistryGame.DTO;
using ChemistryGame.Model;
using System;
using System.Collections.Generic;

namespace ChemistryGame.Infrastructure.Repositories
{
    public class ChemicalElementsRepository
    {
        private readonly ChemicalElementsDBService _chemicalElementsDBService;
        private readonly ChemicalCompoundsDBService _chemicalCompoundsDBService;
        private readonly ChemicalCompoundItemsDBService _chemicalCompoundItemDBService;

        public ChemicalElementsRepository()
        {
            _chemicalElementsDBService = new ChemicalElementsDBService();
            _chemicalCompoundsDBService = new ChemicalCompoundsDBService();
            _chemicalCompoundItemDBService = new ChemicalCompoundItemsDBService();
        }

        public IEnumerable<ChemicalElement> GetAllChemicalElements()
        {
            List<ChemicalElement> result = new List<ChemicalElement>();
            IEnumerable<ChemicalElementDTO> allChemicalElements = _chemicalElementsDBService.Get();
            foreach (var item in allChemicalElements)
            {
                ChemicalElement element = new ChemicalElement
                (
                    id: item.CE_Id,
                    symbol: item.CE_Symbol,
                    atomicMass: Math.Round(item.CE_AtomicMass, 2, MidpointRounding.AwayFromZero),
                    atomicNumber: item.CE_AtomicNumber
                );
                result.Add(element);
            }

            return result;
        }

        public IEnumerable<ChemicalElement> GetElementsOfSelectedCompound(int cc_id)
        {
            List<ChemicalElement> listOfElementsBelongedToSelectedCompoundId = new List<ChemicalElement>();
            IEnumerable<ChemicalElementDTO> elementsWithSignedIndex = _chemicalElementsDBService.GetElementsBelongedToCompound(cc_id);

            foreach (var item in elementsWithSignedIndex)
            {
                ChemicalElement chemicalElement = new ChemicalElement
                (
                    id: item.CE_Id,
                    symbol: item.CE_Symbol,
                    atomicMass: item.CE_AtomicMass,
                    atomicNumber: item.CE_AtomicNumber
                );
              
                listOfElementsBelongedToSelectedCompoundId.Add(chemicalElement);
            }
            return listOfElementsBelongedToSelectedCompoundId;
        }

        public IEnumerable<ChemicalCompoundItem> GetIndexesOfElementsInSelectedCommpounds(int cc_id)
        {
            List<ChemicalCompoundItem> listOfIndexesOfElementsBelongedToSelectedCompoundId = new List<ChemicalCompoundItem>();
            IEnumerable<ChemicalCompoundItemDTO> indexesOfElementsInSignedCompounds = _chemicalCompoundItemDBService.GetIndexBelongedToElementsInCompound(cc_id);

            foreach (var item in indexesOfElementsInSignedCompounds)
            {
                ChemicalCompoundItem chemicalCompoundItem = new ChemicalCompoundItem
                (
                    cc_id: item.CCI_CC_Id,
                    ce_id: item.CCI_CE_Id,
                    index: item.CCI_Index,
                    atoms: item.CCI_Atoms
                );

                listOfIndexesOfElementsBelongedToSelectedCompoundId.Add(chemicalCompoundItem);
            }
            return listOfIndexesOfElementsBelongedToSelectedCompoundId;
        }
    }
}
