﻿using ChemistryGame.Data;
using ChemistryGame.Data.DBServices;
using ChemistryGame.Data.DTO;
using ChemistryGame.DTO;
using ChemistryGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChemistryGame.Infrastructure.Repositories
{
    public class ChemicalCompoundsRepository
    {
        private readonly ChemicalElementsDBService _chemicalElementsDBService;
        private readonly ChemicalCompoundItemsDBService _chemicalCompoundItemsDBService;
        private readonly ChemicalCompoundsViewDBService _chemicalCompoundsViewDBService;

        public ChemicalCompoundsRepository()
        {
            _chemicalElementsDBService = new ChemicalElementsDBService();
            _chemicalCompoundItemsDBService = new ChemicalCompoundItemsDBService();
            _chemicalCompoundsViewDBService = new ChemicalCompoundsViewDBService();
        }

        private static IEnumerable<ChemicalCompoundsViewDTO> _cache = null;

        public IEnumerable<ChemicalCompound> GetCompoundsAvailableInLevel(GameLevel level)
        {
            var result = new List<ChemicalCompound>();

            IEnumerable<ChemicalCompoundsViewDTO> chemicalCompoundsView = GetAllItems();
            IEnumerable<IGrouping<int, ChemicalCompoundsViewDTO>> groupedByCompoundId = chemicalCompoundsView.GroupBy(p => p.CC_Id); //group by chemical compound id

            foreach (var v in groupedByCompoundId)
            {
                bool passedLevelCriteria = ValidateCriteria(v, level);

                if (!passedLevelCriteria)
                {
                    continue;
                }

                //create ChemicalCompound
                ChemicalCompoundsViewDTO chemicalCompoundsViewDTO = v.FirstOrDefault();//get any item
                ChemicalCompound chemicalCompound = new ChemicalCompound
                (
                    id: chemicalCompoundsViewDTO.CC_Id,
                    chemicalFormula: chemicalCompoundsViewDTO.CC_ChemicalFormula,
                    name: chemicalCompoundsViewDTO.CC_Name,
                    isActive: chemicalCompoundsViewDTO.CC_IsActive
                );

                //get items
                IEnumerable<ChemicalCompoundItemDTO> chemicalCompoundItemsDTO = _chemicalCompoundItemsDBService.GetByChemicalCompound(chemicalCompoundsViewDTO.CC_Id);

                var chemicalCompoundItems = new List<ChemicalCompoundItem>();
                foreach (var cei in chemicalCompoundItemsDTO)
                {
                    //get chemical element first
                    ChemicalElementDTO chemicalElementDTO = _chemicalElementsDBService.GetById(cei.CCI_CE_Id);

                    //map/create model object
                    var chemicalElement = new ChemicalElement
                    (
                        id: chemicalElementDTO.CE_Id,
                        symbol: chemicalElementDTO.CE_Symbol,
                        atomicMass: chemicalElementDTO.CE_AtomicMass,
                        atomicNumber: chemicalElementDTO.CE_AtomicNumber
                    );

                    //get chemical compound item
                    var chemicalCompoundItem = new ChemicalCompoundItem
                    (
                        id: cei.CCI_Id,
                        chemicalCompound: chemicalCompound,
                        chemicalElement: chemicalElement,
                        atoms: cei.CCI_Atoms,
                        index: cei.CCI_Index
                    );

                    //add to prepared list
                    chemicalCompoundItems.Add(chemicalCompoundItem);
                }

                //set all items in chemical compound
                chemicalCompound.ChemicalCompoundItems = chemicalCompoundItems;

                //add all chemical compound to final list
                result.Add(chemicalCompound);
            }

            return result;
        }

        private IEnumerable<ChemicalCompoundsViewDTO> GetAllItems()
        {
            if (_cache == null)
            {
                _cache = _chemicalCompoundsViewDBService.Get();
            }

            return _cache;
        }

        private bool ValidateCriteria(IEnumerable<ChemicalCompoundsViewDTO> data, GameLevel gameLevel)
        {
            if (data.GroupBy(p => p.CC_Id).Count() > 1) //expect only data for one compound
            {
                throw new ArgumentException("More than one compound in collection.");
            }

            switch (gameLevel)
            {
                case GameLevel.One:
                    return data.Count() == 2 && data.All(p => p.CCI_Atoms == 1);

                case GameLevel.Two:
                    return data.Count() == 2 && data.Sum(p => p.CCI_Atoms) == 3;

                case GameLevel.Three:
                    return data.Count() == 2 && (data.Sum(p => p.CCI_Atoms) >= 4 && data.Sum(p => p.CCI_Atoms) <= 5);

                case GameLevel.Four:
                    return data.Count() == 3 && (data.Sum(p => p.CCI_Atoms) >= 3 && data.Sum(p => p.CCI_Atoms) <= 5);

                case GameLevel.Five:
                    return (data.Count() == 3 || data.Count() == 4) && (data.Sum(p => p.CCI_Atoms) >= 6 && data.Sum(p => p.CCI_Atoms) <= 10);

                default:
                    return data.Count() == 2 && data.All(p => p.CCI_Atoms == 1);
            }
        }
    }
}
