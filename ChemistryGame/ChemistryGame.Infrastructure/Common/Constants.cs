﻿namespace ChemistryGame.Infrastructure.Common
{
    public class Constants
    {
        public const int INITIAL_COUNT = 6;
        public const int REQUIRED_COMPOUNDS_ON_LEVEL = 10;
        public const int COMPOUNDS_TO_ROLL = 10;
    }
}
