﻿namespace ChemistryGame.Model
{
    public class ChemicalCompoundItem
    {
        public ChemicalCompoundItem(int id, ChemicalCompound chemicalCompound, ChemicalElement chemicalElement, int atoms, int index)
        {
            Id = id;
            ChemicalCompound = chemicalCompound;
            ChemicalElement = chemicalElement;
            Atoms = atoms;
            Index = index;
        }

        public ChemicalCompoundItem(int cc_id, int ce_id, int index, int atoms)
        {
            CC_ID = cc_id;
            CE_ID = ce_id;
            Index = index;
            Atoms = atoms;
        }


        public int Id { get; }
        public ChemicalCompound ChemicalCompound { get; set; }
        public ChemicalElement ChemicalElement { get; set; }
        public int Atoms { get; set; }
        public int Index { get; set; }

        public int CE_ID { get; }
        public int CC_ID { get; }
    }
}
