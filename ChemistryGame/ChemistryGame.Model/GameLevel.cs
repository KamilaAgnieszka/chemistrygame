﻿namespace ChemistryGame.Model
{
    public enum GameLevel
    {
        One,
        Two,
        Three,
        Four,
        Five
    }
}
