﻿using System;

namespace ChemistryGame.Model
{
    public class ChemicalElement : IComparable<ChemicalElement>
    {
        public ChemicalElement(int id, string symbol, decimal atomicMass, int atomicNumber)
        {
            Id = id;
            Symbol = symbol;
            AtomicMass = atomicMass;
            AtomicNumber = atomicNumber;
        }

        public int Id { get; set; }
        public string Symbol { get; set; }
        public decimal AtomicMass { get; set; }
        public int AtomicNumber { get; set; }

        public int ColIdx { get; set; }
        public int RowIdx { get; set; }

        public int CompareTo(ChemicalElement other)
        {
            return AtomicNumber.CompareTo(other.AtomicNumber);
        }


        public override string ToString()
        {
            return Symbol;
        }
    }
}
