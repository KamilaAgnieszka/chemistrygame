﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChemistryGame.Model
{
    public class ChemicalCompound
    {
        public ChemicalCompound(string chemicalFormula, string name, bool isActive, int id)
        {
            Id = id;
            ChemicalFormula = chemicalFormula;
            Name = name;
            IsActive = isActive;
        }

        public string ChemicalFormula { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int Id { get; set; }
        public IEnumerable<ChemicalCompoundItem> ChemicalCompoundItems { get; set; } = new List<ChemicalCompoundItem>();

        public int Points
        {
            get
            {
                var sum = (int)Math.Round(ChemicalCompoundItems.Sum(p => p.Atoms * p.ChemicalElement.AtomicMass));
                return sum;
            }
        }
    }
}