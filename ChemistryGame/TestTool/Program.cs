﻿using ChemistryGame.Data;
using ChemistryGame.Data.Filters;
using System;
using System.Collections.Generic;

namespace TestTool
{
    class Program
    {
        static void Main(string[] args)
        {
            TestFilterComponents();
            Console.ReadKey();
        }

        private static void TestFilterComponents()
        {
            var chemicalCompoundsDBService = new ChemicalCompoundsDBService();
            var compounds = chemicalCompoundsDBService.Get(new List<ChemicalCompoundFilter>
            {
                new ChemicalCompoundFilter("K", 1), 
                new ChemicalCompoundFilter("Cl", 1), 
            });

            foreach(var v in compounds)
            {
                Console.WriteLine(v.CC_ChemicalFormula);
            }
        }
    }
}
