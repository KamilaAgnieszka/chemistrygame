﻿using ImporterOfChemicalCompounds.Infrastructure.Services;
using System;

namespace ImporterOfChemicalCompounds
{
    class Program
    {
        static void Main(string[] args)
        {
            new WikiChemicalDataImporter().ImportData();

            Console.WriteLine("Done.");
            Console.ReadKey();
        }
    }
}
