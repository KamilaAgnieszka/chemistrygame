﻿namespace Elementals.Model
{
    public class HighScore
    {
        public HighScore(string name, int score)
        {
            Name = name;
            Score = score;
        }

        public string Name { get; set; }
        public int Score { get; set; }
    }
}
