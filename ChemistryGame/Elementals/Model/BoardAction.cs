﻿namespace Elementals
{
    public enum BoardAction
    {
        DuplicateElements,
        ReplaceElements,
        CombineElements,
        AddNewElement
    }
}