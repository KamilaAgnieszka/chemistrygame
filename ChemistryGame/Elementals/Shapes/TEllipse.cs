﻿using System;
using System.Drawing;

namespace Elementals
{
    public class TEllipse: TShape
    {
        public TBoard Board;

        public override void DrawShape(Graphics gc)
        {
            gc.FillEllipse(Brushes.Yellow, -1, -1, 2, 2);
        }

        public override void Execute()
        {
            ArrangeInCircle();
        }

        public void ArrangeInCircle()
        {
            var alpha = Board.Alpha;
            for (var i = 0; i < Parent.Children.Count; i++)
            {
                var shape = Parent.Children[i];
                shape.Animation.Reset();
                shape.Animation.Accumulate();
                shape.Origin.X = Math.Min(Board.Width, Board.Height) / 2 - shape.Scale.X;
                shape.Rotation = alpha * i;
                shape.Animation.Accumulate();
                shape.Animation.OnEnd = null;
            }
            Board.Animate();
        }
    }
}
