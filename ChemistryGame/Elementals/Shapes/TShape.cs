﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Elementals
{
    public class TShape
    {
        public List<TShape> Children = new List<TShape>();
        public PointF Origin;
        public PointF Scale = new PointF(1, 1);
        public float Rotation;
        public TAnimation Animation;
        public virtual string Symbol { get; }

        public int Index
        {
            get { return Parent.Children.IndexOf(this); }
            set
            {
                Parent.Children.Remove(this);
                Parent.Children.Insert(value, this);
                Execute();
            }
        }

        private TShape _parent;
        public TShape Parent
        {
            get { return _parent; }
            set
            {
                if (_parent != null) _parent.Children.Remove(this);
                _parent = value;
                if (_parent != null) _parent.Children.Add(this);
            }
        }

        public TShape()
        {
            Animation = new TAnimation(this);
        }

        public Matrix Transform
        {
            get
            {
                var transform = new Matrix();
                transform.Rotate(Rotation);
                transform.Translate(Origin.X, Origin.Y);
                transform.Scale(Scale.X, Scale.Y);
                return transform;
            }
        }

        public virtual void Execute() { }

        public void Assign(TShape shape)
        {
            Origin = shape.Origin;
            Rotation = shape.Rotation;
            Scale = shape.Scale;
        }

        public void Interpolate(TShape startSkel, TShape endSkel, float ratio)
        {
            var rest = 1 - ratio;
            Origin.X = startSkel.Origin.X * rest + endSkel.Origin.X * ratio;
            Origin.Y = startSkel.Origin.Y * rest + endSkel.Origin.Y * ratio;
            Scale.X = startSkel.Scale.X * rest + endSkel.Scale.X * ratio;
            Scale.Y = startSkel.Scale.Y * rest + endSkel.Scale.Y * ratio;
            var delta = endSkel.Rotation - startSkel.Rotation;
            if (delta > 180)
                delta -= 360;
            Rotation = startSkel.Rotation + delta * ratio;
        }

        public PointF TransformPoint(PointF p)
        {
            var pts = new PointF[] { p };
            Transform.TransformPoints(pts);
            return pts[0];
        }

        public void Draw(Graphics gc)
        {
            var actTransform = gc.Transform;
            gc.MultiplyTransform(Transform);
            DrawShape(gc);
            gc.Transform = actTransform;
            var alignment = new StringFormat
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };

            var trans = Transform;
            var font = new Font("Arial", 10, FontStyle.Bold);
            var symbol = Symbol;
            gc.DrawString(symbol, font, Brushes.Black, trans.OffsetX, trans.OffsetY, alignment);
        }

        public virtual void DrawShape(Graphics gc)
        {
            gc.DrawRectangle(Pens.Black, -1, -1, 2, 2);
        }
    }
}
