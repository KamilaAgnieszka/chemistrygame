﻿using ChemistryGame.Model;
using Elementals.Controls;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elementals
{
    public class TOperatorPlus : TEllipse
    {
        private readonly GameService _gameService;

        public TOperatorPlus(GameService gameService)
        {
            this._gameService = gameService;
        }

        public override string Symbol => "+";

        public override void Execute()
        {
            ArrangeInCircle();
            Animation.OnEnd += OnEndAnimation;
        }

        private void OnEndAnimation(object sender, EventArgs e)
        {
            var idx = Index;
            var n = Parent.Children.Count;
            if (idx == 0) return;
            for (int i = -1; i <= 1; i += 2)
            {
                var neigh = Parent.Children[(idx + i) % n];
                neigh.Animation.Reset();
                neigh.Animation.Accumulate();
                neigh.Rotation = Rotation;
                neigh.Animation.Accumulate();
                Children.Add(neigh);
            }
            var compound = Children[0];
            compound.Animation.OnEnd += Collapse;
        }

        private void Collapse(object sender, EventArgs e)
        {
            var compound = Children[0];

            (compound as TChemicalEllipse).ChemicalElements
                .AddRange((Children[1] as TChemicalEllipse).ChemicalElements);

            Parent.Children.Remove(Children[1]);
            Parent.Children.Remove(this);
            ArrangeInCircle();

            compound.Animation.OnEnd += Finalize;
        }

        private void Finalize(object sender, EventArgs e)
        {
            var compound = Children[0];

            List<ChemicalElement> combinedElements = (compound as TChemicalEllipse).ChemicalElements.ToList();

            ChemicalCompound chemicalCompound = _gameService.GetMatchingCompound(combinedElements);

            if (chemicalCompound != null)
            {
                Parent.Children.Remove(compound);
                ArrangeInCircle();

                compound.Animation.OnEnd -= Finalize;

                _gameService.RegisterFinalizedCompound(chemicalCompound);
                _gameService.RollCompoundWhenArranged(chemicalCompound);
            }
        }
    }
}
