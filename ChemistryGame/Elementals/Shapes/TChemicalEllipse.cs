﻿using ChemistryGame.Model;
using System.Collections.Generic;
using System.Drawing;

namespace Elementals.Controls
{
    public class TChemicalEllipse : TEllipse
    {
        public List<ChemicalElement> ChemicalElements = new List<ChemicalElement>();

        public TChemicalEllipse(ChemicalElement chemicalElement)
        {
            ChemicalElements.Add(chemicalElement);
        }

        public TChemicalEllipse(IEnumerable<ChemicalElement> chemicalElements)
        {
            ChemicalElements.AddRange(chemicalElements);
        }

        public override string Symbol { get => string.Join("", ChemicalElements); }

        public void Replace(TChemicalEllipse tChemicalEllipse)
        {
            Index = tChemicalEllipse.Index;
            ArrangeInCircle();
        }

        public void DuplicateAt(TChemicalEllipse secondElement)
        {
            TChemicalEllipse element = new TChemicalEllipse(this.ChemicalElements);
            element.Board = this.Board;
            var shapeScale = 25;
            element.Scale = new PointF(shapeScale, shapeScale);
            element.Parent = Board.Ring;
            element.Index = secondElement.Index+1;
            ArrangeInCircle();
        }
    }
}
