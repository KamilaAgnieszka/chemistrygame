﻿using Elementals.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Elementals
{
    public partial class Form1 : Form
    {
        private frmPeriodicTable frmPeriodicTable = null;

        public Form1()
        {
            InitializeComponent();

            tBoard1.OnFinalizedCompund += (s, e) =>
            {
                RefreshCompoundsList();
                RefreshPoints();
            };

            tBoard1.OnNewGameStarted += (s, e) =>
            {
                RefreshCompoundsList();
                RefreshLevel();
                RefreshPoints();
                RefreshHighScores();
            };

            tBoard1.onLevelChanged += (s, e) =>
            {
                RefreshCompoundsList();
                RefreshLevel();
                RefreshPoints();
            };

            tBoard1.onGameEnd += (s, e) =>
            {
                RefreshHighScores();
            };

            tBoard1.onIncorrectCombine += (s, e) =>
            {
                RefreshPoints();
            };
        }

        private void RefreshPoints()
        {
            lblPoints.Text = tBoard1.Points.ToString();
        }

        private void RefreshLevel()
        {
            lblLevel.Text = tBoard1.Level.ToString();
        }

        private void RefreshCompoundsList()
        {
            compoundsList.Items.Clear();
            compoundsList.Items.AddRange(tBoard1.FinalizedCompounds.Select(p => p.ChemicalFormula).ToArray());
        }

        private void RefreshHighScores()
        {
            lbxHighScores.Items.Clear();
            lbxHighScores.Items.AddRange(tBoard1.HighScores != null ? tBoard1.HighScores.Select(p => $"{p.Name} - {p.Score.ToString()}").ToArray() : new List<string>().ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tBoard1.RestartGame();
        }

        private void showPeriodicTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmPeriodicTable == null)
            {
                frmPeriodicTable = new frmPeriodicTable();
            }

            frmPeriodicTable.Show();
        }

        private void howToPlayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmHowToPlay().ShowDialog();
        }

        private void levelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmLevels().ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAbout().ShowDialog();
        }
    }
}
