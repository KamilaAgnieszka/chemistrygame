﻿namespace Elementals
{
    public struct TElement
    {
        public int Z;
        public int Group;
        public string Symbol;
        public string Name;
    }
}
