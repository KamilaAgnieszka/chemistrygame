﻿using ChemistryGame.Infrastructure.Repositories;
using ChemistryGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elementals
{
    public class GameElementsService
    {
        public static List<ChemicalElement> GetListOfChemcalElements()
        {
            ChemicalElementsRepository chemicalElementsRepository = new ChemicalElementsRepository();
            List<ChemicalElement> list = chemicalElementsRepository.GetAllChemicalElements().ToList();
            list.Sort();

            return list;
        }

        public List<string> GetAllElementSymbols(IEnumerable<ChemicalElement> list)
        {
            List<string> result = new List<string>();

            foreach (var item in list)
            {
                result.Add(item.Symbol);
            }

            return result;
        }

        public IEnumerable<string> GetStartupElementsString(IEnumerable<ChemicalElement> list)//renamed from GetStartupElements
        {

            List<string> result = new List<string>();

            var random = new Random();
            for (int i = 0; i < 6; i++)
            {
                int idxOfElementsArray = random.Next(0, list.Count());
                string t1 = list.ElementAt(idxOfElementsArray).Symbol;
                string t2 = list.ElementAt(idxOfElementsArray).AtomicMass.ToString();
                result.Add(t1);
            }

            return result;
        }

        public IEnumerable<ChemicalCompound> GetLevelElements(GameLevel level)
        {
            ChemicalCompoundsRepository chemicalCompoundsRepository = new ChemicalCompoundsRepository();
            return chemicalCompoundsRepository.GetCompoundsAvailableInLevel(level);
        }
    }
}
