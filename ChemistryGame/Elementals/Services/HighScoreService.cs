﻿using Elementals.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Elementals.Services
{
    public class HighScoreService
    {
        private const string FILE_NAME = "data";
        private readonly string path = Path.Combine(Environment.CurrentDirectory, FILE_NAME);

        public IEnumerable<HighScore> GetHighScores()
        {
            try
            {
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                    File.WriteAllText(path, JsonConvert.SerializeObject(new List<HighScore>()));
                }

                string text = File.ReadAllText(path);

                IEnumerable<HighScore> highScores = JsonConvert.DeserializeObject<IEnumerable<HighScore>>(text) ?? new List<HighScore>();
                return highScores;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new List<HighScore>();
        }

        public void AddNewHighScore(HighScore highScore)
        {
            try
            {
                var highScores = GetHighScores().ToList();

                if (highScores.Count < 10)
                {
                    highScores.Add(highScore);
                }
                else if (highScores.Min(p => p.Score) < highScore.Score)
                {
                    highScores.RemoveAt(9);

                    highScores.Add(highScore);
                }
                else
                {
                    Console.WriteLine("Score is lower than exists.");
                }

                var newHighScore = highScores.OrderByDescending(p => p.Score);

                File.WriteAllText(path, JsonConvert.SerializeObject(newHighScore));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
