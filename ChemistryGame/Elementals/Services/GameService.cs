﻿using ChemistryGame.Infrastructure.Common;
using ChemistryGame.Infrastructure.Repositories;
using ChemistryGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Elementals
{
    public class GameService
    {
        private GameElementsService gameElementsService = new GameElementsService();
        private ChemicalElementsRepository chemicalElementsRepository = new ChemicalElementsRepository();

        private Random random = new Random();
        public ChemicalCompound RolledCompound { get; private set; }

        public IEnumerable<ChemicalCompound> levelChemicalCompounds = new List<ChemicalCompound>();
        public List<ChemicalElement> sixRolledElemets = new List<ChemicalElement>();
        public List<ChemicalElement> tempListOfRestOfRolledCompoud = new List<ChemicalElement>();
        private List<ChemicalElement> firstIndex = new List<ChemicalElement>();
        private List<ChemicalElement> tempFirstIndex = new List<ChemicalElement>();
        private List<ChemicalElement> secondIndex = new List<ChemicalElement>();
        private List<ChemicalElement> tempSecondIndex = new List<ChemicalElement>();
        private List<ChemicalElement> thirdIndex = new List<ChemicalElement>();
        private List<ChemicalElement> tempThirdIndex = new List<ChemicalElement>();
        private List<ChemicalElement> fourthIndex = new List<ChemicalElement>();
        private List<ChemicalElement> tempFourthIndex = new List<ChemicalElement>();
        private IEnumerable<ChemicalElement> startRolledElements = new List<ChemicalElement>();
        private IEnumerable<ChemicalElement> tempStartRolledElements = new List<ChemicalElement>();

        private List<List<ChemicalElement>> tempListOfRolledElements = new List<List<ChemicalElement>>();//temp list of lists of elements with indexes from 1 to 4 (if list included to this list is empty, it will be removed from this list)

        private List<ChemicalCompoundItem> cciList = new List<ChemicalCompoundItem>();
        private List<ChemicalCompoundItem> tempCciList = new List<ChemicalCompoundItem>();

        public List<ChemicalCompound> tenRolledCC = new List<ChemicalCompound>();
        private List<ChemicalCompound> tempTenRolledCC = new List<ChemicalCompound>();
        private List<int> compoundsIsd = new List<int>();
        private List<int> tempCompoundsId = new List<int>();

        public List<ChemicalCompound> FinalizedCompounds { get; private set; } = new List<ChemicalCompound>();

        public int Points { get; private set; } = 0;
        public GameLevel Level { get; private set; } = GameLevel.One;

        public EventHandler onLevelChanged;

        public void InitializeGame()
        {
            SetupLevel(GameLevel.One);
            Points = 0;
        }

        public void SetupLevel(GameLevel level)
        {
            ClearLists();
            Level = level;

            levelChemicalCompounds = gameElementsService.GetLevelElements(level);
            for (int i = 0; i < levelChemicalCompounds.Count(); i++)
            {
                Console.WriteLine(levelChemicalCompounds.ToList()[i].ChemicalFormula);
            }
            //random 10 compounds
            for (int i = 0; i < Constants.COMPOUNDS_TO_ROLL; i++)
            {
                RollCompound(levelChemicalCompounds.ToList());
                tenRolledCC.Add(RolledCompound);
            }
            compoundsIsd = GetTempCompoundsId(tenRolledCC);
            DivideElementsByIndex();
            //split 10 compounds into to lists (original list of all chemical elements from rolled compounds) 
            for (int i = 0; i < cciList.Count(); i++)
            {
                AssignToListByIndex(cciList[i], startRolledElements, firstIndex, secondIndex, thirdIndex, fourthIndex);
            }

            #region Console
            for (int j = 0; j < firstIndex.Count; j++)
            {
                Console.WriteLine("First index array: " + firstIndex[j].Symbol);
            }
            Console.WriteLine();

            for (int j = 0; j < secondIndex.Count; j++)
            {
                Console.WriteLine("Second index array: " + secondIndex[j].Symbol);
            }
            Console.WriteLine();

            for (int j = 0; j < thirdIndex.Count; j++)
            {
                Console.WriteLine("Third index array: " + thirdIndex[j].Symbol);
            }
            Console.WriteLine();

            for (int j = 0; j < fourthIndex.Count; j++)
            {
                Console.WriteLine("Fourth index array: " + fourthIndex[j].Symbol);
            }
            Console.WriteLine();
            #endregion

            tempTenRolledCC = new List<ChemicalCompound>(tenRolledCC);
            //select random 1 //always
            int idx = RollCompound(tempTenRolledCC);
            AddRolledCompoundToLists();
            tempTenRolledCC.RemoveAt(idx);
            tempCompoundsId = GetTempCompoundsId(tempTenRolledCC);
            DivideElementsByIndex_temp();
            for (int i = 0; i < tempCciList.Count(); i++)
            {
                AssignToListByIndex(tempCciList[i], tempStartRolledElements, tempFirstIndex, tempSecondIndex, tempThirdIndex, tempFourthIndex);
            }

            #region Console
            for (int i = 0; i < tempFirstIndex.Count; i++)
            {
                Console.WriteLine("Temp first : " + tempFirstIndex[i].Symbol);
            }
            Console.WriteLine();

            for (int i = 0; i < tempSecondIndex.Count; i++)
            {
                Console.WriteLine("Temp second : " + tempSecondIndex[i].Symbol);
            }
            Console.WriteLine();

            for (int i = 0; i < tempThirdIndex.Count; i++)
            {
                Console.WriteLine("Temp third : " + tempThirdIndex[i].Symbol);
            }
            Console.WriteLine();

            for (int i = 0; i < tempFourthIndex.Count; i++)
            {
                Console.WriteLine("Temp fourth : " + tempFourthIndex[i].Symbol);
            }
            Console.WriteLine();
            #endregion

            AddListOfIdxElementsToListOfRolledElements();

            while (sixRolledElemets.Count() < Constants.INITIAL_COUNT)
            {
                RollElementFromListOfRolledElements();
            }
            #region Console
            for (int i = 0; i < sixRolledElemets.Count(); i++)
            {
                Console.WriteLine("Six : " + sixRolledElemets[i].Symbol);
            }
            Console.WriteLine();

            for (int j = 0; j < tempFirstIndex.Count; j++)
            {
                Console.WriteLine("Reduced first : " + tempFirstIndex[j].Symbol);
            }
            Console.WriteLine();

            for (int j = 0; j < tempSecondIndex.Count; j++)
            {
                Console.WriteLine("Reduced second : " + tempSecondIndex[j].Symbol);
            }
            Console.WriteLine();

            for (int j = 0; j < tempThirdIndex.Count; j++)
            {
                Console.WriteLine("Reduced third : " + tempThirdIndex[j].Symbol);
            }
            Console.WriteLine();

            for (int j = 0; j < tempFourthIndex.Count; j++)
            {
                Console.WriteLine("Reduced fourth : " + tempFourthIndex[j].Symbol);
            }
            Console.WriteLine();
            #endregion

            FinalizedCompounds.Clear();
            onLevelChanged?.Invoke(this, new EventArgs());
        }

        private int RollCompound(List<ChemicalCompound> compoundsList)
        {
            int idxOfLevelCCArray = random.Next(compoundsList.Count());
            RolledCompound = compoundsList.ElementAt(idxOfLevelCCArray);
            Console.WriteLine("Rolled compound: " + RolledCompound.ChemicalFormula);

            return idxOfLevelCCArray;
        }

        private void AddRolledCompoundToLists()
        {
            int atoms = 0;
            //we want always get two first elements which create one compound  //elements have theirs id
            List<ChemicalElement> elementsOfRolledCompound = chemicalElementsRepository.GetElementsOfSelectedCompound(RolledCompound.Id).ToList();
            sixRolledElemets.Add(elementsOfRolledCompound[0]);
            sixRolledElemets.Add(elementsOfRolledCompound[1]);

            IEnumerable<ChemicalCompoundItem> cciOfRolledCompound = chemicalElementsRepository.GetIndexesOfElementsInSelectedCommpounds(RolledCompound.Id);

            for (int i = 0; i < cciOfRolledCompound.Count(); i++)
            {
                atoms += cciOfRolledCompound.ToList()[i].Atoms;
            }

            for (int j = 0; j < cciOfRolledCompound.Count(); j++)
            {
                if (cciOfRolledCompound.ToList()[j].Index == 1 || cciOfRolledCompound.ToList()[j].Index == 2)
                {
                    if (cciOfRolledCompound.ToList()[j].Atoms > 1)
                    {
                        for (int i = 1; i < cciOfRolledCompound.ToList()[j].Atoms; i++)
                        {
                            AssignToListByIndex_RolledCompound(cciOfRolledCompound.ToList()[j], elementsOfRolledCompound, tempFirstIndex, tempSecondIndex, tempThirdIndex, tempFourthIndex);
                        }
                    }
                }
                else
                {
                    AssignToListByIndex(cciOfRolledCompound.ToList()[j], elementsOfRolledCompound, tempFirstIndex, tempSecondIndex, tempThirdIndex, tempFourthIndex);
                }
            }
        }

        private List<int> GetTempCompoundsId(List<ChemicalCompound> ccList)
        {
            List<int> compoId = new List<int>();
            for (int i = 0; i < ccList.Count(); i++)
            {
                compoId.Add(ccList[i].Id);
            }
            return compoId;
        }

        private void DivideElementsByIndex()
        {
            //there are all elements which included in ten rolled compounds and cci ids 
            for (int i = 0; i < compoundsIsd.Count(); i++)
            {
                cciList = cciList.Concat(chemicalElementsRepository.GetIndexesOfElementsInSelectedCommpounds(compoundsIsd[i])).ToList();
                startRolledElements = startRolledElements.Concat(chemicalElementsRepository.GetElementsOfSelectedCompound(compoundsIsd[i]));
            }
        }

        private void DivideElementsByIndex_temp()
        {
            //elements which are rolled, but not showed on board 
            for (int i = 0; i < tempCompoundsId.Count(); i++)
            {
                tempCciList = tempCciList.Concat(chemicalElementsRepository.GetIndexesOfElementsInSelectedCommpounds(tempCompoundsId[i])).ToList();
                tempStartRolledElements = tempStartRolledElements.Concat(chemicalElementsRepository.GetElementsOfSelectedCompound(tempCompoundsId[i]));
            }
        }


        private void AddListOfIdxElementsToListOfRolledElements()
        {
            if (tempFirstIndex.Count != 0)
            {
                tempListOfRolledElements.Add(tempFirstIndex);
            }

            if (tempSecondIndex.Count != 0)
            {
                tempListOfRolledElements.Add(tempSecondIndex);
            }

            if (tempThirdIndex.Count != 0)
            {
                tempListOfRolledElements.Add(tempThirdIndex);
            }

            if (tempFourthIndex.Count != 0)
            {
                tempListOfRolledElements.Add(tempFourthIndex);
            }

            tempListOfRolledElements = new List<List<ChemicalElement>>(tempListOfRolledElements);
        }

        private void RollElementFromListOfRolledElements()
        {
            int idxOfListWithElements = random.Next(tempListOfRolledElements.Count());
            List<ChemicalElement> elementsList = tempListOfRolledElements[idxOfListWithElements];

            if (elementsList.Count() > 0)
            {
                int idxOfElementsList = random.Next(elementsList.Count());
                sixRolledElemets.Add(elementsList.ElementAt(idxOfElementsList));
                elementsList.RemoveAt(idxOfElementsList);
            }
        }

        /// <summary>
        /// use it to roll new element on board
        /// </summary>
        /// <returns></returns>
        public ChemicalElement RollElement()
        {
            ChemicalElement rolledElement = null;
            bool isListEmpty = false;

            while (rolledElement == null && tempListOfRolledElements.Count() > 0 && !isListEmpty)
            {
                int idxOfListWithElements = GetRandom();
                List<ChemicalElement> tempElementsList = tempListOfRolledElements[idxOfListWithElements];
                if (tempListOfRolledElements[idxOfListWithElements].Count() > 0)
                {
                    int idxOfElementsList = random.Next(tempListOfRolledElements[idxOfListWithElements].Count());
                    rolledElement = tempListOfRolledElements[idxOfListWithElements].ElementAt(idxOfElementsList);
                    tempListOfRolledElements[idxOfListWithElements].RemoveAt(idxOfElementsList);
                }
                else
                {
                    tempListOfRolledElements[idxOfListWithElements] = null;
                    for (int i = 0; i < tempListOfRolledElements.Count(); i++)
                    {
                        if (tempListOfRolledElements[i] != null)
                        {
                            break;
                        }
                        isListEmpty = true;
                    }
                }
            }

            return rolledElement;
        }

        private int GetRandom()
        {
            int idxOfListWithElements = random.Next(tempListOfRolledElements.Count());
            while (tempListOfRolledElements[idxOfListWithElements] == null)
            {
                idxOfListWithElements = random.Next(tempListOfRolledElements.Count());
            }
            return idxOfListWithElements;
        }

        public bool IsGameOver()
        {
            if (tempListOfRolledElements.Count() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void AssignToListByIndex(ChemicalCompoundItem item,
            IEnumerable<ChemicalElement> startRolledElements_,
            List<ChemicalElement> firstIndex_,
            List<ChemicalElement> secondIndex_,
            List<ChemicalElement> thirdIndex_,
            List<ChemicalElement> fourthIndex_)
        {
            ChemicalCompoundItem cci = item;
            int ce_id = cci.CE_ID;
            int index = cci.Index;
            int atoms = cci.Atoms;

            var element = startRolledElements_.FirstOrDefault(x => x.Id == ce_id);

            if (index == 1)
            {
                for (int i = 0; i < atoms; i++)
                {
                    firstIndex_.Add(element);
                }
            }
            else if (index == 2)
            {
                for (int i = 0; i < atoms; i++)
                {
                    secondIndex_.Add(element);
                }
            }
            else if (index == 3)
            {
                for (int i = 0; i < atoms; i++)
                {
                    thirdIndex_.Add(element);
                }
            }
            else if (index == 4)
            {
                for (int i = 0; i < atoms; i++)
                {
                    fourthIndex_.Add(element);
                }
            }
        }

        private void AssignToListByIndex_RolledCompound(ChemicalCompoundItem item,
            IEnumerable<ChemicalElement> tempStartRolledElements,
            List<ChemicalElement> firstIndex_,
            List<ChemicalElement> secondIndex_,
            List<ChemicalElement> thirdIndex_,
            List<ChemicalElement> fourthIndex_)
        {
            ChemicalCompoundItem cci = item;
            int ce_id = cci.CE_ID;
            int index = cci.Index;
            int atoms = cci.Atoms;

            var element = tempStartRolledElements.FirstOrDefault(x => x.Id == ce_id);

            if (index == 1)
            {
                firstIndex_.Add(element);
            }
            else if (index == 2)
            {
                secondIndex_.Add(element);
            }
            else if (index == 3)
            {
                thirdIndex_.Add(element);
            }
            else if (index == 4)
            {
                fourthIndex_.Add(element);
            }
        }

        public bool CanCombine(List<ChemicalElement> chemicalElements)
        {
            var grByElementSymbol = chemicalElements.GroupBy(q => q.Symbol).Select(p => new { Symbol = p.Key, Count = p.Count() });

            IEnumerable<ChemicalCompound> equalCount = tenRolledCC;

            foreach (var v in grByElementSymbol)
            {
                equalCount = equalCount.Where(p => p.ChemicalCompoundItems
                        .Any(q => q.ChemicalElement.Symbol == v.Symbol && q.Atoms >= v.Count));
            }

            if (equalCount.Any() == false)
            {
                equalCount = levelChemicalCompounds;
                foreach (var v in grByElementSymbol)
                {
                    equalCount = equalCount.Where(p => p.ChemicalCompoundItems
                            .Any(q => q.ChemicalElement.Symbol == v.Symbol && q.Atoms >= v.Count));
                }
            }

            return equalCount.Any();
        }

        public ChemicalCompound GetMatchingCompound(List<ChemicalElement> chemicalElements)
        {
            Console.WriteLine("check matching compound for: ");
            foreach (var item in chemicalElements)
            {
                Console.WriteLine(item.Symbol);
            }
            var grByElementSymbol = chemicalElements.GroupBy(q => q.Symbol).Select(p => new { Symbol = p.Key, Count = p.Count() });
            IEnumerable<ChemicalCompound> equalCount = levelChemicalCompounds;

            foreach (var v in grByElementSymbol)
            {
                equalCount = equalCount.Where(p => p.ChemicalCompoundItems
                        .Any(q => q.ChemicalElement.Symbol == v.Symbol && q.Atoms == v.Count));
            }

            Console.WriteLine("matching compound: ");
            foreach (var item in equalCount)
            {
                Console.WriteLine(item.ChemicalFormula);
            }

            var result = new List<ChemicalCompound>();
            if (equalCount.Count() > 0)
            {
                foreach (ChemicalCompound compound in equalCount)
                {
                    var compoundItems = compound.ChemicalCompoundItems.Select(p => new { Symbol = p.ChemicalElement.Symbol, Count = p.Atoms });
                    bool allMatch = compoundItems.All(p => grByElementSymbol.Any(q => q.Symbol == p.Symbol && q.Count == p.Count));

                    if (allMatch)
                    {
                        result.Add(compound);
                    }
                }
            }

            Console.WriteLine("final matching compound: ");
            foreach (var item in result)
            {
                Console.WriteLine(item.ChemicalFormula);
            }
            return result.FirstOrDefault();
        }


        public EventHandler<FinalizedCompundEventAgs> OnFinalizedCompund;
        public void RegisterFinalizedCompound(ChemicalCompound chemicalCompound)
        {
            FinalizedCompounds.Add(chemicalCompound);
            Points += chemicalCompound.Points;

            OnFinalizedCompund?.Invoke(this, new FinalizedCompundEventAgs(chemicalCompound));
        }

        public EventHandler<EventArgs> OnIncorrectCombine;
        public void RegisterIncorrectCombine(IEnumerable<ChemicalElement> chemicalElements)
        {
            var sum = (int)Math.Round(chemicalElements.Sum(p => p.AtomicMass));
            Points = Math.Max(Points - sum, 0);
        }

        private void ClearLists()
        {
            levelChemicalCompounds.ToList().Clear();
            sixRolledElemets.Clear();
            tempListOfRestOfRolledCompoud.Clear();
            firstIndex.Clear();
            tempFirstIndex.Clear();
            secondIndex.Clear();
            tempSecondIndex.Clear();
            thirdIndex.Clear();
            tempThirdIndex.Clear();
            fourthIndex.Clear();
            tempFourthIndex.Clear();
            startRolledElements.ToList().Clear();
            tempStartRolledElements.ToList().Clear();
            tempListOfRolledElements.Clear();
            cciList.Clear();
            tempCciList.Clear();
            tenRolledCC.Clear();
            tempTenRolledCC.Clear();
            compoundsIsd.Clear();
            tempCompoundsId.Clear();
        }

        public void RollCompoundWhenArranged(ChemicalCompound chemicalCompound)
        {
            tenRolledCC.Remove(chemicalCompound);
            //because when we remove compound from list we can remove more the same compoundt if they was rolled
            while (tenRolledCC.Count < 10)
            {
                RollCompound(levelChemicalCompounds.ToList());
                tenRolledCC.Add(RolledCompound);
                tempTenRolledCC.Add(RolledCompound);
                int id = RolledCompound.Id;
                List<ChemicalCompoundItem> cci = chemicalElementsRepository.GetIndexesOfElementsInSelectedCommpounds(id).ToList();
                IEnumerable<ChemicalElement> elements = chemicalElementsRepository.GetElementsOfSelectedCompound(id).ToList();

                for (int i = 0; i < cci.Count(); i++)
                {
                    AssignToListByIndex(cci[i], elements, tempFirstIndex, tempSecondIndex, tempThirdIndex, tempFourthIndex);
                }
            }
        }
    }
}
