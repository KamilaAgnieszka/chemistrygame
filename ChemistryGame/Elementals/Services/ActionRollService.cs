﻿using System;
using System.Collections.Generic;

namespace Elementals.Services
{
    public class ActionRollService
    {
        private int RandomCounter { get; set; }//define on which place will be "+" in the queue
        private int Counter { get; set; } = 0;
        private Random random = new Random();
        private List<BoardAction> RingOperators = new List<BoardAction> { BoardAction.CombineElements, BoardAction.CombineElements, BoardAction.CombineElements, BoardAction.DuplicateElements, BoardAction.ReplaceElements };
        public BoardAction RolledAction { get; private set; }

        public ActionRollService()
        {
            SetCounter();
        }

        private void SetCounter()
        {
            var idx = new Random();
            RandomCounter = idx.Next(1, 1);
        }

        public BoardAction RollAction()
        {
            if (Counter != RandomCounter)
            {
                RolledAction = BoardAction.AddNewElement;
                Counter++;
            }
            else
            {
                var random = new Random();
                int idxOfOperatorsArray = random.Next(RingOperators.Count);

                RolledAction = RingOperators[idxOfOperatorsArray];
                Counter = 0;
                SetCounter();
            }

            Console.WriteLine($"Rolled action: {RolledAction}");
            return RolledAction;
        }
    }
}
