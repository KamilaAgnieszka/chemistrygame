﻿using ChemistryGame.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Elementals
{
    public class TPeriodicTable : DataGridView
    {
        public int ColumnWidth = 10;
        public float Fold = 0.1f;
        public static int[] ElectronCount = new int[] { 0, 2, 8, 8, 18, 18, 32, 32 };
        public static int[] NobleZ = new int[ElectronCount.Length];//Mass number of noble gases
        private List<ChemicalElement> Elements;
        private System.ComponentModel.IContainer components;
        public static int ShellsCount = 4;

        public TPeriodicTable()
        {
            DoubleBuffered = true;
            ColumnHeadersVisible = false;
            RowHeadersVisible = false;
            AllowUserToResizeRows = false;
            AllowUserToResizeColumns = false;
            CellPainting += TPeriodicTable_CellPainting;
            ReadOnly = true;
            Elements = GameElementsService.GetListOfChemcalElements();
            ColumnCount = 32;
            RowCount = 8;

            for (int i = 0; i < ColumnCount; i++)
            {
                Columns[i].Width = 30;
                Columns[i].HeaderText = i.ToString();
            }

            for (int i = 0; i < RowCount; i++)
            {
                Rows[i].Height = 30;
            }

            for (int i = 1; i < ElectronCount.Count(); i++)
            {
                NobleZ[i] = NobleZ[i - 1] + ElectronCount[i];
            }

            var rowIdx = 0;
            for (int i = 0; i < Elements.Count(); i++)
            {
                if (i + 1 > NobleZ[rowIdx + 1]) rowIdx++;
                var colIdx = i - NobleZ[rowIdx];
                if (colIdx > (rowIdx + 1) / 2)
                {
                    colIdx += ColumnCount - ElectronCount[rowIdx + 1];
                }
                this[colIdx, rowIdx].Value = Elements[i];

                Elements[i].RowIdx = rowIdx;
                Elements[i].ColIdx = colIdx;
            }
        }

        private void TPeriodicTable_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            e.Handled = true;
            var box = e.CellBounds;
            e.Graphics.FillRectangle(Brushes.White, box);
            e.Graphics.DrawRectangle(Pens.Black, box);

            var element = this[e.ColumnIndex, e.RowIndex].Value as ChemicalElement;
            if (element != null)
            {
                var symbolFont = new Font("Arial", 10);
                e.Graphics.DrawString(element.Symbol, symbolFont, Brushes.Black, box.Location);
                if (e.ColumnIndex > ShellsCount - 2)
                {
                    return;
                }

                var rc = new Rectangle(0, 0, box.Width / 3, box.Height / 3);
                rc.Offset(box.X + box.Width - rc.Width, box.Y);
                e.Graphics.DrawRectangle(Pens.Black, rc);

                rc.Inflate(-2, -2);
                var center = new Point(rc.X + rc.Width / 2, rc.Y + rc.Height / 2);
                e.Graphics.DrawLine(Pens.Black, rc.X, center.Y, rc.X + rc.Width, center.Y);

                if (Columns[e.ColumnIndex].HeaderText == "-")
                {
                    e.Graphics.DrawLine(Pens.Black, center.X, rc.Y, center.X, rc.Y + rc.Height);
                }
            }
        }

        protected override void OnScroll(ScrollEventArgs e)
        {
            base.OnScroll(e);
            Invalidate();
        }

        protected override void OnCellDoubleClick(DataGridViewCellEventArgs e)
        {

            base.OnCellDoubleClick(e);
            var idx = e.ColumnIndex;
            if (idx > ShellsCount - 2) return;

            if (Columns[idx].HeaderText == "-")
                Columns[idx].HeaderText = "+";
            else
                Columns[idx].HeaderText = "-";

            var rowIdx = e.RowIndex;
            var element = this[idx, rowIdx].Value;

            var prevElement = element;
            while (prevElement != null)
            {
                element = prevElement;
                rowIdx--;
                if (rowIdx < 0) break;
                prevElement = this[e.ColumnIndex, rowIdx].Value;
            }

            var startElement = (ChemicalElement)element;
            if (startElement == null) return;
            var nextElement = Elements[startElement.AtomicNumber];
            for (int i = startElement.ColIdx + 1; i < nextElement.ColIdx; i++)
            {

                Columns[i].Visible = !Columns[i].Visible;
            }
        }


        protected override void OnCellFormatting(DataGridViewCellFormattingEventArgs e)
        {
            base.OnCellFormatting(e);

            DataGridViewCell cell = this[e.ColumnIndex, e.RowIndex];

            ChemicalElement chemicalElement = cell.Value as ChemicalElement;

            if (chemicalElement != null)
            {
                cell.ToolTipText = CreateTooltip(chemicalElement);
            }
        }

        private string CreateTooltip(ChemicalElement e)
        {
            return string.Format($"{e.Symbol} {Environment.NewLine}Z: {e.AtomicNumber}{Environment.NewLine}A: {e.AtomicMass}");
        }
    }
}
