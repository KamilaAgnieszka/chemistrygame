﻿using ChemistryGame.Model;
using System;

namespace Elementals
{
    public class FinalizedCompundEventAgs : EventArgs
    {
        public FinalizedCompundEventAgs(ChemicalCompound chemicalCompound)
        {
            ChemicalCompound = chemicalCompound;
        }

        public ChemicalCompound ChemicalCompound { get; }
    }
}
