﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Elementals.Services;
using Elementals.Factories;
using Elementals.Controls;
using ChemistryGame.Model;
using ChemistryGame.Infrastructure.Common;
using Elementals.Model;
using Elementals.Forms;

namespace Elementals
{
    public partial class TBoard : UserControl
    {
        public TShape Ring = new TShape();

        public float Alpha { get { return 360f / Ring.Children.Count; } }
        private GameService gameService { get; set; } = new GameService();
        private readonly HighScoreService highScoreService = new HighScoreService();
        public int ShapeScale { get; set; } = 25;
        public bool isFirstElementSelected = false;
        public bool isSecondElementSelected = false;
        public int? firstElementPosition;
        public int? secondElementPosition;
        private GameElementsService gameElementService;
        private TEllipseFactory tShapeFactory;
        private TEllipse Center;
        ActionRollService actionRollService;

        public List<ChemicalCompound> FinalizedCompounds { get { return gameService.FinalizedCompounds; } }
        public int Points { get { return gameService.Points; } }
        public GameLevel Level { get { return gameService.Level; } }
        public IEnumerable<HighScore> HighScores { get { return highScoreService.GetHighScores(); } }

        public bool IsInitializing { get; private set; } = true;

        public EventHandler<FinalizedCompundEventAgs> OnFinalizedCompund;
        public EventHandler OnNewGameStarted;
        public EventHandler onLevelChanged;
        public EventHandler onGameEnd;
        public EventHandler onIncorrectCombine;

        public TBoard()
        {
            InitializeComponent();

            gameService.InitializeGame();
            actionRollService = new ActionRollService();
            gameElementService = new GameElementsService();
            tShapeFactory = new TEllipseFactory(actionRollService, this, gameService);

            DoubleBuffered = true;

            gameService.OnFinalizedCompund += OnFinalizedCompound;
            gameService.onLevelChanged += OnLevelChanged;
            OnNewGameStarted?.Invoke(this, new EventArgs());
        }

        public void RestartGame()
        {
            InitializeComponent();

            Center = null;
            Ring = new TShape();
            gameService = new GameService();
            gameService.InitializeGame();

            actionRollService = new ActionRollService();
            gameElementService = new GameElementsService();
            tShapeFactory = new TEllipseFactory(actionRollService, this, gameService);

            IsInitializing = true;

            TBoard_Load(this, null);

            gameService.OnFinalizedCompund += OnFinalizedCompound;
            gameService.onLevelChanged += OnLevelChanged;
            OnNewGameStarted?.Invoke(this, new EventArgs());
        }

        private void OnFinalizedCompound(object sender, FinalizedCompundEventAgs e)
        {
            if (gameService.FinalizedCompounds.Count == Constants.REQUIRED_COMPOUNDS_ON_LEVEL)
            {
                if (gameService.Level != GameLevel.Five)
                {
                    StartNextLevel(gameService.Level + 1);
                }
                else
                {
                    RegisterHighScore();
                    onGameEnd?.Invoke(this, new EventArgs());
                }
            }

            OnFinalizedCompund?.Invoke(this, e);
        }

        private void RegisterHighScore()
        {
            var frm = new frmEnterName();

            var dialogResult = frm.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                var highScoreService = new HighScoreService();

                var scores = highScoreService.GetHighScores();

                foreach (var v in scores)
                {
                    Console.WriteLine($"{v.Name} {v.Score}");
                }

                highScoreService.AddNewHighScore(new HighScore(frm.EnteredName, Points));
            }
        }

        private void StartNextLevel(GameLevel gameLevel)
        {
            gameService.SetupLevel(gameLevel);

            Ring.Children.Clear();
            Center = null;

            List<TEllipse> stratupEl = tShapeFactory.CreateStartupElements(Constants.INITIAL_COUNT);
            foreach (var item in stratupEl)
            {
                if (Center != null)
                {
                    Center.Parent = Ring;
                }
                Center = item;
            }

            AddCentralElement();
            Ring.Children[0].Execute();

            IsInitializing = false;
        }

        private void OnLevelChanged(object sender, EventArgs e)
        {
            onLevelChanged?.Invoke(this, new EventArgs());
        }

        private void TBoard_Load(object sender, EventArgs e)
        {
            List<TEllipse> stratupEl = tShapeFactory.CreateStartupElements(Constants.INITIAL_COUNT);
            foreach (var item in stratupEl)
            {
                if (Center != null)
                {
                    Center.Parent = Ring;
                }
                Center = item;
            }

            AddCentralElement();
            Ring.Children[0].Execute();

            IsInitializing = false;

            OnNewGameStarted?.Invoke(this, new EventArgs());
        }

        public bool AddCentralElement()
        {
            if (Center != null)
                Center.Parent = Ring;//sign central element to Center.Parent

            Center = tShapeFactory.CreateNew();

            return Center != null;
        }

        private void AnimateTimer_Tick(object sender, EventArgs e)
        {
            for (var i = 0; i < Ring.Children.Count; i++)
                Ring.Children[i].Animation.Animate();
            var enabled = false;
            for (var i = 0; i < Ring.Children.Count; i++)
                enabled |= Ring.Children[i].Animation.Enabled;
            if (!enabled)
                AnimateTimer.Stop();
            Invalidate();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (gameService.IsGameOver())
            {
                Console.WriteLine("Game over!");
                return;
            }
            base.OnMouseClick(e);
            var alpha = Alpha;
            var beta = 180 * Math.Atan2(e.Y - Height / 2, e.X - Width / 2) / Math.PI;
            if (beta < 0) beta += 360;

            if (Center is TOperatorDuplicate || Center is TOperatorReplace)
            {
                int clickedPosition = (int)(beta / alpha);

                CheckSecondClick(clickedPosition);
            }
            else
            {
                if (Center is TOperatorPlus)
                {
                    int clickedPosition = (int)(beta / alpha);
                    TChemicalEllipse firstElement = Ring.Children[clickedPosition] as TChemicalEllipse;

                    int secoundElementIndex = clickedPosition + 1 == Ring.Children.Count ? 0 : clickedPosition + 1;
                    TChemicalEllipse secondElement = Ring.Children[secoundElementIndex] as TChemicalEllipse;

                    List<ChemicalElement> elementsList = new List<ChemicalElement>(firstElement.ChemicalElements);
                    elementsList.AddRange(secondElement.ChemicalElements);

                    bool canCombine = gameService.CanCombine(elementsList);
                    if (!canCombine)
                    {
                        MessageBox.Show("Can not combine these elements!", "Warning", MessageBoxButtons.OK);
                        gameService.RegisterIncorrectCombine(elementsList);
                        onIncorrectCombine?.Invoke(this, new EventArgs());
                        return;
                    }
                }

                bool added = AddCentralElement();
                Ring.Children.Last().Index = (int)(beta / alpha) + 1;

                CheckGameOver(added);
            }
        }

        private void CheckGameOver(bool added)
        {
            if (gameService.IsGameOver() || !added)
            {
                if (MessageBox.Show("Game over. Do you want to play angain?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    RestartGame();
                }
                else
                {
                    Console.WriteLine("koniec gry");
                    // user clicked no
                }
            }
        }

        public void CheckSecondClick(int clickedPosition)
        {
            if (isFirstElementSelected == false)
            {
                isFirstElementSelected = true;
                firstElementPosition = clickedPosition;
            }
            else if (isFirstElementSelected == true && isSecondElementSelected == false)
            {
                isFirstElementSelected = false;
                secondElementPosition = clickedPosition;

                TChemicalEllipse firstElement = Ring.Children[firstElementPosition.Value] as TChemicalEllipse;
                TChemicalEllipse secondElement = Ring.Children[secondElementPosition.Value] as TChemicalEllipse;

                if (Center is TOperatorDuplicate)
                {
                    firstElement.DuplicateAt(secondElement);
                }
                else
                {
                    firstElement.Replace(secondElement);
                }

                Center = null;
                bool added = AddCentralElement();
                CheckGameOver(added);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.TranslateTransform(Width / 2, Height / 2);

            for (var i = 0; i < Ring.Children.Count; i++)//do not draw last shape because it is the middle shape
            {
                Ring.Children[i].Draw(e.Graphics);
            }
            Center?.Draw(e.Graphics);
        }

        public void Animate()
        {
            AnimateTimer.Start();
        }
    }
}
