﻿using System;
using System.Collections.Generic;

namespace Elementals
{
    public class TAnimation
    {
        public bool Enabled;
        public List<TShape> Keys = new List<TShape>();
        int KeyNo;
        int FrameNo;
        int FramesCount = 15;
        public TShape Control;

        public EventHandler OnEnd;

        public void Animate()
        {
            if (Enabled)
            {
                if (KeyNo < Keys.Count - 1)
                {
                    if (FrameNo < FramesCount)
                    {
                        Control.Interpolate(Keys[KeyNo], Keys[KeyNo + 1], (float)FrameNo / (FramesCount - 1));
                        FrameNo++;
                    }
                    else
                    {
                        FrameNo = 0;
                        KeyNo++;
                    }
                }
                else
                {
                    KeyNo = 0;
                    Enabled = false;
                    if (OnEnd != null)
                    {
                        OnEnd(this, EventArgs.Empty);
                    }

                }
            }

        }

        public void Reset()
        {
            Keys.Clear();
            KeyNo = 0;
            Enabled = true;
        }

        public void Accumulate()
        {
            var skeleton = new TShape();
            skeleton.Assign(Control);
            Keys.Add(skeleton);
            Control.Assign(Keys[0]);
        }

        public TAnimation(TShape control)
        {
            Control = control;
        }
    }
}
