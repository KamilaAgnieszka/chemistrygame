﻿using ChemistryGame.Model;
using Elementals.Controls;
using Elementals.Services;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Elementals.Factories
{
    public class TEllipseFactory
    {
        private readonly ActionRollService _actionRollService;
        private readonly TBoard _tBoard;
        private readonly GameService _gameService;

        public TEllipseFactory(ActionRollService actionRollService, TBoard tBoard, GameService gameService)
        {
            _actionRollService = actionRollService;
            _tBoard = tBoard;
            _gameService = gameService;
        }

        public List<TEllipse> CreateStartupElements(int count)
        {
            List<TEllipse> result = new List<TEllipse>();
            for (int i = 0; i < count; i++)
            {
                TEllipse element = new TEllipse();
                ChemicalElement rolledElement = _gameService.sixRolledElemets[i];
                element = new TChemicalEllipse(rolledElement);
                element.Board = _tBoard;
                var shapeScale = 25;
                element.Scale = new PointF(shapeScale, shapeScale);

                result.Add(element);
            }
            return result;
        }

        public TEllipse CreateNew()
        {
            TEllipse element = new TEllipse();
            BoardAction rolledAction = _actionRollService.RollAction();

            switch (rolledAction)
            {
                case BoardAction.DuplicateElements:
                    element = new TOperatorDuplicate();
                    break;
                case BoardAction.ReplaceElements:
                    element = new TOperatorReplace();
                    break;
                case BoardAction.CombineElements:
                    element = new TOperatorPlus(_gameService);
                    element.Board = _tBoard;
                    break;
                case BoardAction.AddNewElement:
                    {
                        ChemicalElement rolledElement = _gameService.RollElement();

                        if (rolledElement == null)
                        {
                            return null;
                        }

                        element = new TChemicalEllipse(rolledElement);
                        element.Board = _tBoard;
                        Console.WriteLine("centerElement: " + rolledElement.Symbol);
                        break;
                    }
                default:
                    throw new NotImplementedException($"Not supported type {rolledAction.ToString()}.");
            }

            var shapeScale = 25;
            element.Scale = new PointF(shapeScale, shapeScale);

            return element;
        }
    }
}
