﻿using System;
using System.Windows.Forms;

namespace Elementals.Forms
{
    public partial class frmEnterName : Form
    {
        public string EnteredName { get { return tbxName.Text; } }

        public frmEnterName()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(tbxName.Text))
            {
                MessageBox.Show("Enter name");
                return;
            }

            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}