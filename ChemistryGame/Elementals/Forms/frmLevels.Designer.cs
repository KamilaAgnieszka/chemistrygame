﻿namespace Elementals.Forms
{
    partial class frmLevels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLevels));
            this.tbxLevels = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // tbxLevels
            // 
            this.tbxLevels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxLevels.Location = new System.Drawing.Point(0, 0);
            this.tbxLevels.Name = "tbxLevels";
            this.tbxLevels.ReadOnly = true;
            this.tbxLevels.Size = new System.Drawing.Size(716, 144);
            this.tbxLevels.TabIndex = 0;
            this.tbxLevels.Text = "";
            // 
            // frmLevels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 144);
            this.Controls.Add(this.tbxLevels);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmLevels";
            this.Text = "Levels";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox tbxLevels;
    }
}