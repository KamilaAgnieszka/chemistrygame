﻿namespace Elementals
{
    partial class frmPeriodicTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPeriodicTable));
            this.tPeriodicTable1 = new Elementals.TPeriodicTable();
            ((System.ComponentModel.ISupportInitialize)(this.tPeriodicTable1)).BeginInit();
            this.SuspendLayout();
            // 
            // tPeriodicTable1
            // 
            this.tPeriodicTable1.AllowUserToAddRows = false;
            this.tPeriodicTable1.AllowUserToResizeColumns = false;
            this.tPeriodicTable1.AllowUserToResizeRows = false;
            this.tPeriodicTable1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tPeriodicTable1.ColumnHeadersVisible = false;
            this.tPeriodicTable1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tPeriodicTable1.Location = new System.Drawing.Point(0, 0);
            this.tPeriodicTable1.Name = "tPeriodicTable1";
            this.tPeriodicTable1.ReadOnly = true;
            this.tPeriodicTable1.RowHeadersVisible = false;
            this.tPeriodicTable1.RowHeadersWidth = 20;
            this.tPeriodicTable1.Size = new System.Drawing.Size(970, 236);
            this.tPeriodicTable1.TabIndex = 3;
            // 
            // frmPeriodicTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 236);
            this.Controls.Add(this.tPeriodicTable1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPeriodicTable";
            this.Text = "Periodic table";
            ((System.ComponentModel.ISupportInitialize)(this.tPeriodicTable1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TPeriodicTable tPeriodicTable1;
    }
}