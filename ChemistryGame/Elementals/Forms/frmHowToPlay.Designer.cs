﻿namespace Elementals.Forms
{
    partial class frmHowToPlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHowToPlay));
            this.tbxHowToPlay = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // tbxHowToPlay
            // 
            this.tbxHowToPlay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxHowToPlay.Location = new System.Drawing.Point(0, 0);
            this.tbxHowToPlay.Name = "tbxHowToPlay";
            this.tbxHowToPlay.ReadOnly = true;
            this.tbxHowToPlay.Size = new System.Drawing.Size(800, 274);
            this.tbxHowToPlay.TabIndex = 0;
            this.tbxHowToPlay.Text = "";
            // 
            // frmHowToPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 274);
            this.Controls.Add(this.tbxHowToPlay);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmHowToPlay";
            this.Text = "How to play";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox tbxHowToPlay;
    }
}