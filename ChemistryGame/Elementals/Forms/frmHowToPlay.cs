﻿using System.IO;
using System.Windows.Forms;

namespace Elementals.Forms
{
    public partial class frmHowToPlay : Form
    {
        public frmHowToPlay()
        {
            InitializeComponent();

            tbxHowToPlay.Text = File.ReadAllText("Files//HowToPlay");
        }
    }
}
