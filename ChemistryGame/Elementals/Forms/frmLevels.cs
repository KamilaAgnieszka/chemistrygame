﻿using System.IO;
using System.Windows.Forms;

namespace Elementals.Forms
{
    public partial class frmLevels : Form
    {
        public frmLevels()
        {
            InitializeComponent();
            tbxLevels.Text = File.ReadAllText("Files//Levels");
        }
    }
}
