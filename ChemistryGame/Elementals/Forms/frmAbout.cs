﻿using System.IO;
using System.Windows.Forms;

namespace Elementals.Forms
{
    public partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
            tbxAbout.Text = File.ReadAllText("Files//About");
        }
    }
}
