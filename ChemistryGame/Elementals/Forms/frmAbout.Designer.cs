﻿namespace Elementals.Forms
{
    partial class frmAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAbout));
            this.tbxAbout = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // tbxAbout
            // 
            this.tbxAbout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxAbout.Location = new System.Drawing.Point(0, 0);
            this.tbxAbout.Name = "tbxAbout";
            this.tbxAbout.ReadOnly = true;
            this.tbxAbout.Size = new System.Drawing.Size(462, 42);
            this.tbxAbout.TabIndex = 0;
            this.tbxAbout.Text = "";
            // 
            // frmAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 42);
            this.Controls.Add(this.tbxAbout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAbout";
            this.Text = "About";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox tbxAbout;
    }
}