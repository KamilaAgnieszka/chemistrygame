﻿using ImporterOfChemicalCompounds.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImporterOfChemicalCompounds.Infrastructure.Services
{
    public class WikiChemicalElementsService
    {
        public const string url = "https://en.wikipedia.org/wiki/List_of_chemical_elements";

        public IEnumerable<WikiChemicalElement> GetAllElements()
        {
            // HtmlWeb is a helper class to get pages from the web
            var web = new HtmlAgilityPack.HtmlWeb();

            // Create an HtmlDocument from the contents found at given url
            var doc = web.Load(url);

            var xpath = "/html[1]/body[1]/div[3]/div[3]/div[4]/div[1]/table[1]/tbody[1]/tr"; // find the `h2` element that has a span with the id, 'Deployments' (the header)

            // Get the elements from the specified XPath
            var elements = doc.DocumentNode.SelectNodes(xpath);

            foreach (var v in elements.Skip(2))
            {
                var values = v.ChildNodes.Where(p => p.Name == "td").ToList();

                if (values.Count < 7)
                {
                    continue;
                }

                string symbol = values[1].InnerText;
                string massText = GetMass(values);

                decimal atomicMass = Convert.ToDecimal(massText);

                //read atomic number
                string atomicNumberText = values[0].InnerText;
                int atomicNumber = Int32.Parse(atomicNumberText);

                yield return new WikiChemicalElement
                {
                    Symbol = symbol,
                    AtomicMass = atomicMass,
                    AtomicNumber = atomicNumber
                };
            }
        }

        private static string GetMass(List<HtmlAgilityPack.HtmlNode> values)
        {
            var massText = values[6].InnerText;

            if (massText.Contains("&"))
            {
                massText = massText.Remove(massText.IndexOf('&'));
            }

            if (massText.Contains("("))
            {
                massText = massText.Remove(massText.IndexOf('('));
            }

            if (massText.StartsWith("["))
            {
                massText = massText.Remove(0, 1);
                massText = massText.Remove(massText.IndexOf(']'));
            }

            return massText;
        }
    }
}
