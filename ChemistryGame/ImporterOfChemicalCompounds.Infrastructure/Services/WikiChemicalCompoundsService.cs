﻿using HtmlAgilityPack;
using ImporterOfChemicalCompounds.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImporterOfChemicalCompounds.Infrastructure.Services
{
    public class WikiChemicalCompoundsService
    {
        public const string url = "https://pl.wikipedia.org/wiki/Zwi%C4%85zki_chemiczne_wed%C5%82ug_pierwiastk%C3%B3w";

        public IEnumerable<WikiChemicalCompound> GetAllElements()
        {
            var result = new List<WikiChemicalCompound>();

            // HtmlWeb is a helper class to get pages from the web
            var web = new HtmlAgilityPack.HtmlWeb();

            // Create an HtmlDocument from the contents found at given url
            var doc = web.Load(url);

            var xpath = "//*[@id=\"mw-content-text\"]/div/p"; // find the `h2` element that has a span with the id, 'Deployments' (the header)

            // Get the elements from the specified XPath
            var elements = doc.DocumentNode.SelectNodes(xpath);

            var elementsWithoutSkipped = elements
                .Skip(1); //first contains header

            foreach (HtmlNode v in elementsWithoutSkipped)
            {
                var childNodes = v.ChildNodes;

                foreach (var p in childNodes)
                {
                    //second element contains not important data, so skip every second item (indexing from 0!)
                    if (childNodes.IndexOf(p) % 2 != 0)
                    {
                        continue;
                    }

                    if (!IsSupported(p))
                    {
                        Console.WriteLine($"Element {p.InnerText} is not supported.");
                        continue;
                    }

                    string formula = p.InnerText;
                    string name = ExtractName(p);
                    IEnumerable<WikiChemicalCompoundItem> items = ExtractItems(p);

                    var wikiChemicalCompound = new WikiChemicalCompound
                    {
                        ChemicalFormula = formula,
                        Name = name,
                        Elements = items
                    };

                    //wiki page contains duplications (i.e. for O and H exists H2O for both elements)
                    //duplications will be skiped
                    if (!result.Any(q => q.ChemicalFormula == wikiChemicalCompound.ChemicalFormula))
                    {
                        result.Add(wikiChemicalCompound);
                    }
                }
            }

            return result;
        }

        private bool IsSupported(HtmlNode htmlNode)
        {
            return NotSupportedList.All(p => !htmlNode.InnerText.Contains(p));
        }

        private List<string> NotSupportedList = new List<string>
        {
            "·",
            "+", //C5H14NO+
            "</sup>", //
            "C88H97Cl2N9O33", //not supported <a href="/wiki/Teikoplanina" title="Teikoplanina">C<sub>88</sub>H<sub>97</sub>C<sub>l2</sub>N<sub>9</sub>O<sub>33</sub></a>
            "C56H68Cl4CuN16S4",
            "C17H14Cl2F2N2O3",
            "C18H13N1O5/8/11S1/2/3Na1/2/3",
            "C56H87N016",
            "C11H15D2NO3",
            "C7H140",
            //"[(C6H7O2)(OOCCH3)3]n",
            "[",
            "]",
            "(",
            "]",
            "-",
            "Cl2H",
            "D2O"
        };

        private IEnumerable<WikiChemicalCompoundItem> ExtractItems(HtmlNode v)
        {
            var result = new List<WikiChemicalCompoundItem>();

            HtmlNode[] children = v.ChildNodes.ToArray();

            for (int i = 0; i < children.Count(); i++)
            {
                HtmlNode currentItem = children[i];

                //if value
                if (currentItem.Name == "sub") //sub contains value (ie 3). We will skip that becaouse has been proceed in last iteration (in previous element ie C)
                {
                    continue;
                }

                int nextNodeAtomsValue = 1;

                if (i + 1 < children.Count())//get next value Na2Cl - will get '2' when proceed 'Na'. Validation if no next value (after Cl is no value)
                {
                    for (int j = i; j < children.Count(); j++)
                    {
                        if (Int32.TryParse(children[i + 1].InnerText, out int parseResult))
                        {
                            nextNodeAtomsValue = parseResult;
                            break;
                        }

                    }
                }

                if (ContainsSingle(currentItem.InnerText))
                {
                    string symbol = currentItem.InnerText;

                    //if next contains value
                    //get value
                    result.Add(new WikiChemicalCompoundItem()
                    {
                        Symbol = symbol.Trim(),
                        NumberOfAtoms = nextNodeAtomsValue //if last item = pust next node value with atoms count
                    });
                }
                else
                {
                    IEnumerable<string> symbols = ExtractAllSymbols(currentItem.InnerText);

                    foreach (var s in symbols)
                    {
                        result.Add(new WikiChemicalCompoundItem()
                        {
                            Symbol = s.Trim(),
                            NumberOfAtoms = symbols.Last() != s ? 1 : nextNodeAtomsValue
                        });
                    }
                }


            }

            return result;
        }

        private bool ContainsSingle(string symbolText)
        {
            return symbolText.Count(p => char.IsUpper(p)) == 1;//Na - only one upper; NaC - two upper so contains many
        }

        private IEnumerable<string> ExtractAllSymbols(string symbolText)
        {
            var result = new List<string>();

            string readySymbol = symbolText[0].ToString();

            for (int i = 1; i < symbolText.Count(); i++)
            {
                //if new upper
                // add to list ready symbol
                if (char.IsUpper(symbolText[i]))
                {
                    result.Add(readySymbol);
                    readySymbol = symbolText[i].ToString();
                }
                else
                {
                    readySymbol += symbolText[i].ToString();
                }
            }

            result.Add(readySymbol);//add last created item (loop will skip that)

            return result;
        }

        private string ExtractName(HtmlNode p)
        {
            return p.Attributes["title"].Value;
        }
    }
}