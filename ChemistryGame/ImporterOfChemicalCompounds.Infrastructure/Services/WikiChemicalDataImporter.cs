﻿using ChemistryGame.Data;
using ChemistryGame.Data.DTO;
using ChemistryGame.DTO;
using ImporterOfChemicalCompounds.Infrastructure.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImporterOfChemicalCompounds.Infrastructure.Services
{
    public class WikiChemicalDataImporter
    {
        private readonly ChemicalCompoundsDBService _chemicaCompoundsDBService;
        private readonly ChemicalElementsDBService _chemicalElementsDBService;
        private readonly ChemicalCompoundItemsDBService _chemicalCompoundItemsDBService;

        public WikiChemicalDataImporter()
        {
            _chemicalElementsDBService = new ChemicalElementsDBService();
            _chemicaCompoundsDBService = new ChemicalCompoundsDBService();
            _chemicalCompoundItemsDBService = new ChemicalCompoundItemsDBService();
        }

        public void ImportData()
        {
            //first - get and insert into table chemical elements
            ImportChemicalElements();

            //then get all compounds from source and insert into table
            ImportChemicalCompunds();
        }

        private void ImportChemicalCompunds()
        {
            var wikiChemicalCompoundsService = new WikiChemicalCompoundsService();
            IEnumerable<WikiChemicalCompound> wikiChemicalCompounds = wikiChemicalCompoundsService.GetAllElements();

            //get all chemical elements (we need Id's for each)
            IEnumerable<ChemicalElementDTO> chemicalElements = _chemicalElementsDBService.Get();

            foreach (var v in wikiChemicalCompounds)
            {
                var chemicalCompound = new ChemicalCompoundDTO
                {
                    CC_Name = v.Name,
                    CC_ChemicalFormula = v.ChemicalFormula
                };

                Console.WriteLine($"Started: Insert chemical compound into table. Data: {JsonConvert.SerializeObject(chemicalCompound)}");
                int chemicalCompoundId = _chemicaCompoundsDBService.Add(chemicalCompound);
                Console.WriteLine($"Done: Insert element into table. Inserterd Id: {chemicalCompoundId}");

                int counter = 1;
                foreach (var item in v.Elements)
                {
                    var chemicalCompoundItem = new ChemicalCompoundItemDTO
                    {
                        CCI_CE_Id = chemicalElements.First(p => p.CE_Symbol == item.Symbol).CE_Id,
                        CCI_CC_Id = chemicalCompoundId,
                        CCI_Atoms = item.NumberOfAtoms,
                        CCI_Index = counter
                    };

                    //insert into table
                    Console.WriteLine($"Started: Insert chemical compound item into table. Data: {JsonConvert.SerializeObject(chemicalCompoundItem)}");
                    _chemicalCompoundItemsDBService.Add(chemicalCompoundItem);
                    Console.WriteLine($"Done: Insert element into table.");

                    counter++;
                }

                Console.WriteLine(JsonConvert.SerializeObject(v, Formatting.Indented)); //log
            }
        }

        private void ImportChemicalElements()
        {
            //get elements from wiki
            Console.WriteLine("Started: Get data from Wiki.");
            IEnumerable<WikiChemicalElement> chemicalElements = new WikiChemicalElementsService().GetAllElements();
            Console.WriteLine($"Done: Get data from Wiki. Received items count: {chemicalElements.Count()}");

            //insert elements to DB
            foreach (var v in chemicalElements.OrderBy(p => p.Symbol))
            {
                Console.WriteLine($"Processing Wiki entry: {JsonConvert.SerializeObject(v)}");

                var dbChemicalElement = new ChemicalElementDTO()
                {
                    CE_Symbol = v.Symbol,
                    CE_AtomicMass = v.AtomicMass,
                    CE_AtomicNumber = v.AtomicNumber
                };

                Console.WriteLine($"Started: Insert element into table. Data: {JsonConvert.SerializeObject(v)}");
                _chemicalElementsDBService.Add(dbChemicalElement);
                Console.WriteLine($"Done: Insert element into table.");
            }
        }
    }
}