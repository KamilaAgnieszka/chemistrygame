﻿namespace ImporterOfChemicalCompounds.Infrastructure.Model
{
    public class WikiChemicalElement
    {
        public string Symbol { get; set; }
        public decimal AtomicMass { get; set; }
        public int AtomicNumber { get; set; }
    }
}
