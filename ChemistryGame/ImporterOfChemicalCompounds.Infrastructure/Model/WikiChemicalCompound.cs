﻿using System.Collections.Generic;

namespace ImporterOfChemicalCompounds.Infrastructure.Model
{
    public class WikiChemicalCompound
    {
        public string ChemicalFormula { get; set; }
        public string Name { get; set; }
        public IEnumerable<WikiChemicalCompoundItem> Elements { get; set; }
    }
}
