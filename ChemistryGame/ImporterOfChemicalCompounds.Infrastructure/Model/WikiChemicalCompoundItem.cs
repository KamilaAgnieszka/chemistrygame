﻿namespace ImporterOfChemicalCompounds.Infrastructure.Model
{
    public class WikiChemicalCompoundItem
    {
        public string Symbol { get; set; }
        public int NumberOfAtoms { get; set; }
    }
}