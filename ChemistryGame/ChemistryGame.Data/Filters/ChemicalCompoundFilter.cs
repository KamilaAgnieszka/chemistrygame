﻿namespace ChemistryGame.Data.Filters
{
    public class ChemicalCompoundFilter
    {
        public ChemicalCompoundFilter(string symbol, int atoms)
        {
            Symbol = symbol;
            Atoms = atoms;
        }

        public string Symbol { get; set; }
        public int Atoms { get; set; }
    }
}
