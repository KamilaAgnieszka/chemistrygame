﻿using ChemistryGame.Data.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ChemistryGame.Data.DBServices
{
    public class ChemicalCompoundsViewDBService
    {
        public IEnumerable<ChemicalCompoundsViewDTO> Get()
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"SELECT *       
                                FROM   ChemicalCompoundItems INNER JOIN
                                       ChemicalCompounds ON ChemicalCompoundItems.CCI_CC_Id = ChemicalCompounds.CC_Id INNER JOIN
                                       ChemicalElements ON ChemicalCompoundItems.CCI_CE_Id = ChemicalElements.CE_Id"
            };

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable);
        }

        public IEnumerable<ChemicalCompoundsViewDTO> Get(string chemicalElementSymbol)
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"SELECT *       
                                FROM   ChemicalCompoundItems INNER JOIN
                                       ChemicalCompounds ON ChemicalCompoundItems.CCI_CC_Id = ChemicalCompounds.CC_Id INNER JOIN
                                       ChemicalElements ON ChemicalCompoundItems.CCI_CE_Id = ChemicalElements.CE_Id
                                WHERE  CE_Symbol = @CE_Symbol"
            };
            myCommand.Parameters.Add(new SqlParameter("CE_Symbol", chemicalElementSymbol));

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable);
        }

        private IEnumerable<ChemicalCompoundsViewDTO> ExtractDTO(DataTable dataTable)
        {
            var result = new List<ChemicalCompoundsViewDTO>();

            foreach (DataRow row in dataTable.Rows)
            {
                ChemicalCompoundsViewDTO chemicalCompoundsViewDTO = new ChemicalCompoundsViewDTO();

                chemicalCompoundsViewDTO.CCI_Id = Convert.ToInt32(row["CCI_Id"].ToString());
                chemicalCompoundsViewDTO.CCI_CC_Id = Convert.ToInt32(row["CCI_CC_Id"].ToString());
                chemicalCompoundsViewDTO.CCI_CE_Id = Convert.ToInt32(row["CCI_CE_Id"].ToString());
                chemicalCompoundsViewDTO.CCI_Atoms = Convert.ToInt32(row["CCI_Atoms"].ToString());
                chemicalCompoundsViewDTO.CC_Id = Convert.ToInt32(row["CC_Id"].ToString());
                chemicalCompoundsViewDTO.CC_ChemicalFormula = row["CC_ChemicalFormula"].ToString();
                chemicalCompoundsViewDTO.CC_Name = row["CC_Name"].ToString();
                chemicalCompoundsViewDTO.CC_IsActive = Convert.ToBoolean(row["CC_IsActive"].ToString());
                chemicalCompoundsViewDTO.CE_Id = Convert.ToInt32(row["CE_Id"].ToString());
                chemicalCompoundsViewDTO.CE_Symbol = row["CE_Symbol"].ToString();
                chemicalCompoundsViewDTO.CE_AtomicMass = Convert.ToDecimal(row["CE_AtomicMass"].ToString());

                result.Add(chemicalCompoundsViewDTO);
            }

            return result;
        }
    }
}
