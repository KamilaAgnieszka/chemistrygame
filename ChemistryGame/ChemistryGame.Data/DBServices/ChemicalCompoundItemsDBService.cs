﻿using ChemistryGame.Data.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ChemistryGame.Data
{
    public class ChemicalCompoundItemsDBService
    {
        private const string TABLE_NAME = "ChemicalCompoundItems";

        public void ClearTable()
        {
            SqlCommand myCommand = new SqlCommand($"delete from {TABLE_NAME}");

            try
            {
                DBService.ExecuteCommand(myCommand);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void Add(ChemicalCompoundItemDTO chemicalCompoundItem)
        {
            SqlCommand myCommand = new SqlCommand($"INSERT INTO {TABLE_NAME} " +
                $"(CCI_CC_Id, CCI_CE_Id, CCI_Atoms, CCI_Index) " +
                 "Values (@CCI_CC_Id, @CCI_CE_Id, @CCI_Atoms, @CCI_Index)");

            myCommand.Parameters.Add(new SqlParameter("CCI_CC_Id", chemicalCompoundItem.CCI_CC_Id));
            myCommand.Parameters.Add(new SqlParameter("CCI_CE_Id", chemicalCompoundItem.CCI_CE_Id));
            myCommand.Parameters.Add(new SqlParameter("CCI_Atoms", chemicalCompoundItem.CCI_Atoms));
            myCommand.Parameters.Add(new SqlParameter("CCI_Index", chemicalCompoundItem.CCI_Index));

            DBService.ExecuteCommand(myCommand);
        }

        public IEnumerable<ChemicalCompoundItemDTO> Get()
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $"select * from {TABLE_NAME}"
            };

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);
            return ExtractDTO(dataTable);
        }

        public IEnumerable<ChemicalCompoundItemDTO> GetByChemicalCompound(int cc_Id)
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"SELECT * from {TABLE_NAME}      
                                 where CCI_CC_Id = @CCI_CC_Id"
            };

            myCommand.Parameters.Add(new SqlParameter("CCI_CC_Id", cc_Id));

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable);
        }

        public IEnumerable<ChemicalCompoundItemDTO> GetIndexBelongedToElementsInCompound(int cc_id)
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"select cci.*
                              from ChemicalCompounds cc, ChemicalCompoundItems cci, ChemicalElements ce
                              where ce.CE_Id = cci.CCI_CE_Id and cc.CC_Id=cci.CCI_CC_Id and
                              cc.CC_Id = @CC_Id"
            };

            myCommand.Parameters.Add(new SqlParameter("CC_Id", cc_id));

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);
            return ExtractDTO(dataTable);
        }

        private IEnumerable<ChemicalCompoundItemDTO> ExtractDTO(DataTable dataTable)
        {
            var result = new List<ChemicalCompoundItemDTO>();

            foreach (DataRow row in dataTable.Rows)
            {
                ChemicalCompoundItemDTO chemicalCompoundItemDTO = new ChemicalCompoundItemDTO();

                chemicalCompoundItemDTO.CCI_Id = Convert.ToInt32(row["CCI_Id"].ToString());
                chemicalCompoundItemDTO.CCI_CC_Id = Convert.ToInt32(row["CCI_CC_Id"].ToString());
                chemicalCompoundItemDTO.CCI_CE_Id = Convert.ToInt32(row["CCI_CE_Id"].ToString());
                chemicalCompoundItemDTO.CCI_Atoms = Convert.ToInt32(row["CCI_Atoms"].ToString());
                chemicalCompoundItemDTO.CCI_Index = Convert.ToInt32(row["CCI_Index"].ToString());

                result.Add(chemicalCompoundItemDTO);
            }

            return result;
        }
    }
}
