﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ChemistryGame.Data
{
    public static class DBService
    {
        private static SqlConnection CreateConnection()
        {
            return new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ChemicalCompoundsDB"].ConnectionString);
        }

        public static void ExecuteCommand(SqlCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            SqlConnection connection = CreateConnection();

            command.Connection = connection;

            connection.Open();

            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static DataTable ExecuteReadCommand(SqlCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            SqlConnection connection = CreateConnection();
            command.Connection = connection;

            connection.Open();

            try
            {

                SqlDataReader sqlDataReader = command.ExecuteReader();

                //create datatable to close connection and return data
                var dataTable = new DataTable();
                dataTable.Load(sqlDataReader);
                return dataTable;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}