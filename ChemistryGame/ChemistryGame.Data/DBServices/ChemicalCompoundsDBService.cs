﻿using ChemistryGame.Data.DTO;
using ChemistryGame.Data.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ChemistryGame.Data
{
    public class ChemicalCompoundsDBService
    {
        private const string TABLE_NAME = "ChemicalCompounds";

        public void ClearTable()
        {
            SqlCommand myCommand = new SqlCommand($"delete from {TABLE_NAME}");

            try
            {
                DBService.ExecuteCommand(myCommand);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public int Add(ChemicalCompoundDTO chemicalCompound)
        {
            SqlCommand myCommand = new SqlCommand($"INSERT INTO {TABLE_NAME} (CC_ChemicalFormula, CC_Name, CC_IsActive) " +
                    "Values (@CC_ChemicalFormula, @CC_Name, @CC_IsActive); SELECT SCOPE_IDENTITY()");

            myCommand.Parameters.Add(new SqlParameter("CC_ChemicalFormula", chemicalCompound.CC_ChemicalFormula));
            myCommand.Parameters.Add(new SqlParameter("CC_Name", chemicalCompound.CC_Name));
            myCommand.Parameters.Add(new SqlParameter("CC_IsActive", chemicalCompound.CC_IsActive));

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return Convert.ToInt32(dataTable.Rows[0][0]); //return inserted ID
        }

        public IEnumerable<ChemicalCompoundDTO> Get()
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $"select * from {TABLE_NAME}"
            };

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable);
        }

        public IEnumerable<ChemicalCompoundDTO> GetByChemicalElement(string chemicalElementSymbol)
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"SELECT *       
                                FROM   ChemicalCompoundItems INNER JOIN
                                       ChemicalCompounds ON ChemicalCompoundItems.CCI_CC_Id = ChemicalCompounds.CC_Id INNER JOIN
                                       ChemicalElements ON ChemicalCompoundItems.CCI_CE_Id = ChemicalElements.CE_Id
                                where CE_Symbol = @CE_Symbol"
            };

            myCommand.Parameters.Add(new SqlParameter("CE_Symbol", chemicalElementSymbol));

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable);
        }

        public IEnumerable<ChemicalCompoundDTO> Get(IEnumerable<ChemicalCompoundFilter> filters)
        {
            var filtersList = filters.ToList();

            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"SELECT distinct ChemicalCompounds.*          
                                FROM   ChemicalCompoundItems INNER JOIN
                                       ChemicalCompounds ON ChemicalCompoundItems.CCI_CC_Id = ChemicalCompounds.CC_Id INNER JOIN
                                       ChemicalElements ON ChemicalCompoundItems.CCI_CE_Id = ChemicalElements.CE_Id
                                WHERE "
            };

            var filtersSubCommands = new List<string>();
            foreach (var v in filters)
            {
                int indexOfFilter = filtersList.IndexOf(v);

                filtersSubCommands.Add($@" CC_Id in (SELECT CC_Id     
                                FROM   ChemicalCompoundItems INNER JOIN
                                       ChemicalCompounds ON ChemicalCompoundItems.CCI_CC_Id = ChemicalCompounds.CC_Id INNER JOIN
                                       ChemicalElements ON ChemicalCompoundItems.CCI_CE_Id = ChemicalElements.CE_Id 
                                WHERE  CE_Symbol = @CE_Symbol{indexOfFilter} AND 
                                      
                                       CCI_Atoms >= @CCI_Atoms{indexOfFilter})");

                myCommand.Parameters.Add(new SqlParameter($"@CE_Symbol{indexOfFilter}", v.Symbol));
                myCommand.Parameters.Add(new SqlParameter($"@CCI_Atoms{indexOfFilter}", v.Atoms));
            }

            myCommand.CommandText += string.Join(" AND ", filtersSubCommands);

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable);
        }

        public IEnumerable<ChemicalCompoundDTO> ExtractDTO(DataTable dataTable)
        {
            var result = new List<ChemicalCompoundDTO>();

            foreach (DataRow row in dataTable.Rows)
            {
                ChemicalCompoundDTO chemicalCompoundDTO = new ChemicalCompoundDTO();

                chemicalCompoundDTO.CC_Id = Convert.ToInt32(row["CC_Id"].ToString());
                chemicalCompoundDTO.CC_ChemicalFormula = row["CC_ChemicalFormula"].ToString();
                chemicalCompoundDTO.CC_Name = row["CC_Name"].ToString();
                chemicalCompoundDTO.CC_IsActive = Convert.ToBoolean(row["CC_IsActive"].ToString());

                result.Add(chemicalCompoundDTO);
            }

            return result;
        }
    }
}