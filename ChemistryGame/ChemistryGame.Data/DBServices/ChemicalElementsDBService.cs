﻿using ChemistryGame.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ChemistryGame.Data
{
    public class ChemicalElementsDBService
    {
        private const string TABLE_NAME = "ChemicalElements";

        public void ClearTable()
        {
            SqlCommand myCommand = new SqlCommand($"delete from {TABLE_NAME}");

            try
            {
                DBService.ExecuteCommand(myCommand);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void Add(ChemicalElementDTO chemicalElement)
        {
            SqlCommand myCommand = new SqlCommand($"INSERT INTO {TABLE_NAME} (CE_Symbol, CE_AtomicMass, CE_AtomicNumber) " +
                    "Values (@Symbol, @AtomicMass, @AtomicNumber)");
            myCommand.Parameters.Add(new SqlParameter("Symbol", chemicalElement.CE_Symbol));
            myCommand.Parameters.Add(new SqlParameter("AtomicMass", chemicalElement.CE_AtomicMass));
            myCommand.Parameters.Add(new SqlParameter("AtomicNumber", chemicalElement.CE_AtomicNumber));

            DBService.ExecuteCommand(myCommand);
        }

        public IEnumerable<ChemicalElementDTO> Get()
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $"select * from {TABLE_NAME}"
            };

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable);
        }

        public ChemicalElementDTO GetById(int ce_Id)
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"SELECT * from {TABLE_NAME}      
                                 where CE_Id = @CE_Id"
            };

            myCommand.Parameters.Add(new SqlParameter("CE_Id", ce_Id));

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);

            return ExtractDTO(dataTable).FirstOrDefault();
        }

        private static List<ChemicalElementDTO> ExtractDTO(DataTable dataTable)
        {
            var result = new List<ChemicalElementDTO>();
            foreach (DataRow row in dataTable.Rows)
            {
                ChemicalElementDTO chemicalElement = new ChemicalElementDTO();

                chemicalElement.CE_Id = Convert.ToInt32(row["CE_Id"].ToString());
                chemicalElement.CE_Symbol = row["CE_Symbol"].ToString();
                chemicalElement.CE_AtomicMass = Convert.ToDecimal(row["CE_AtomicMass"].ToString());
                chemicalElement.CE_AtomicNumber = Convert.ToInt32(row["CE_AtomicNumber"].ToString());

                result.Add(chemicalElement);
            }

            return result;
        }

        public IEnumerable<ChemicalElementDTO> GetElementsBelongedToCompound(int cc_id)
        {
            SqlCommand myCommand = new SqlCommand()
            {
                CommandText = $@"select distinct ce.*, cci.cci_index
                              from ChemicalCompounds cc, ChemicalCompoundItems cci, ChemicalElements ce
                              where ce.CE_Id = cci.CCI_CE_Id and cc.CC_Id=cci.CCI_CC_Id and
                              cc.CC_Id = @CC_Id
                              order by cci.cci_index"
            };

            myCommand.Parameters.Add(new SqlParameter("CC_Id", cc_id));

            DataTable dataTable = DBService.ExecuteReadCommand(myCommand);
            return ExtractDTO(dataTable);
        }
    }
}
