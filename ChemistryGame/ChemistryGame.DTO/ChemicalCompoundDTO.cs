﻿namespace ChemistryGame.Data.DTO
{
    public class ChemicalCompoundDTO
    {
        public int CC_Id { get; set; }
        public string CC_ChemicalFormula { get; set; }
        public string CC_Name { get; set; }
        public bool CC_IsActive { get; set; }
    }
}