﻿namespace ChemistryGame.Data.DTO
{
    public class ChemicalCompoundsViewDTO
    {
        public int CC_Id { get; set; }
        public string CC_ChemicalFormula { get; set; }
        public string CC_Name { get; set; }
        public bool CC_IsActive { get; set; }
        public int CCI_Id { get; set; }
        public int CCI_Atoms { get; set; }
        public int CCI_CC_Id { get; set; }
        public int CCI_CE_Id { get; set; }
        public int CE_Id { get; set; }
        public string CE_Symbol { get; set; }
        public decimal CE_AtomicMass { get; set; }
    }
}