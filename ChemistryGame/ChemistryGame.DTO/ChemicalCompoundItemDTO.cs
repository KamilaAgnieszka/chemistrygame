﻿namespace ChemistryGame.Data.DTO
{
    public class ChemicalCompoundItemDTO
    {
        public int CCI_Id { get; set; }
        public int CCI_Atoms { get; set; }
        public int CCI_CC_Id { get; set; }
        public int CCI_CE_Id { get; set; }
        public int CCI_Index { get; set; }
    }
}