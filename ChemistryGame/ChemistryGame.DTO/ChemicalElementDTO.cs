﻿namespace ChemistryGame.DTO
{
    public class ChemicalElementDTO
    {
        public int CE_Id { get; set; }
        public string CE_Symbol { get; set; }
        public decimal CE_AtomicMass { get; set; }
        public int CE_AtomicNumber { get; set; }
    }
}
